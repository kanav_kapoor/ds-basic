<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>DS Basic - Mug</title>
		<!-- CSS -->
		
		<!-- plugins -->
		<link rel="stylesheet" href="assets/lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.min.css">
		<link href='http://fonts.googleapis.com/css?family=Josefin+Sans|Shadows+Into+Light|Pacifico|Quicksand|Architects+Daughter|Dancing+Script|Chewy|Gloria+Hallelujah|Shadows+Into+Light|Poiret+One|Indie+Flower|Oswald|' rel='stylesheet' type='text/css'>

		<!-- <link rel="stylesheet" href="assets/css/fonts.css"> -->
		<link rel="stylesheet" href="assets/css/menu.css">
		<!--/ plugins -->

		<link rel="stylesheet" href="assets/css/index.css">
		<link rel="stylesheet" href="assets/css/media.css">
		<!--/ CSS -->
	</head>
	<body>
		<div id="loader">
			<p class="text-center"><br>Loading.... Please wait...</p>
		</div>
		<!-- header -->
		<header>
			<!-- Nav -->
			<nav class="animate focus">
				<ul class="nav nav-pills nav-stacked tool-options" role="tablist">
					<li role="presentation" class="active"><a class="text-center" title="Products" data-toggle="tab" aria-controls="tabpane-products" href="#tabpane-products"><i class="fa fa-th"></i></a></li>
					<li role="presentation"><a class="text-center" title="Product Options" data-toggle="tab" aria-controls="tabpane-product-options" href="#tabpane-product-options"><i class="fa fa-adjust"></i></a></li>
					<li role="presentation"><a class="text-center" title="Text Options" data-toggle="tab" aria-controls="tabpane-text-options" href="#tabpane-text-options"><i class="fa fa-pencil"></i></a></li>
					<li role="presentation"><a class="text-center" title="Image Options" data-toggle="tab" aria-controls="tabpane-image-options" href="#tabpane-image-options"><i class="fa fa-picture-o"></i></a></li>
					<li role="presentation"><a class="text-center" title="Price Calculation" data-toggle="tab" aria-controls="tabpane-price-calculation-options" href="#tabpane-price-calculation-options"><i class="fa fa-cart-plus"></i></a></li>
				</ul>
			</nav>
			<!--/ Nav -->
			<div class="nav-controller hidden">
				<span class="[ ion-2x ion-navicon-round ] controller-open"></span>
				<span class="[ ion-2x ion-ios-close-outline ] controller-close"></span>
			</div>
		</header>
		<!--/ header -->

		<!--  Main Container -->
		<div class="content-wrapper container-fluid">
			<div class="row">

				<!-- Tools -->
				<div class="row tools">
					<div class="col-sm-8 col-md-10 col-sm-offset-10 col-md-offset-10">
						<div class="btn-group btn-group-justified" role="group" aria-label="">
							<div class="btn-group" role="group">
								<button title="Reset" type="button" data-toggle="confirmation" class="btn btn-default" id="reset"><i class="ion-2x ion-ios-loop"></i></button>
							</div>
							<div class="btn-group" role="group">
								<button title="Save" type="button" data-toggle="tooltip" data-placement="bottom" title="Save" class="btn btn-default" id="saveProduct"><i class="ion-2x ion-ios-heart-outline"></i></i></button>
							</div>
						</div>
					</div>
				</div>
				<!--/ Tools -->

				<!-- Design Tool -->
				<div class="row">
					<!-- Left -->
					<div class="col-sm-10 col-md-10">
						<div class="col-sm-16 col-md-14 col-lg-12">
							<!-- Tab Panes -->
							<div class="tab-content tool-panes">
								<!-- Products -->
								<div role="tabpanel" class="products tab-pane fade in active" id="tabpane-products">
									<p class="custom-pane-header text-center h3">Products</p><hr>
									<div class="row display-gallery products-list">
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-01/mug-thumbnail.png" alt="mug " title="mug">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-01/left-preview.png", "base": "assets/img/products/mug-01/left-base.png","template": [{"type": "text", "params": {"src": "PEACOCK ", "editorMode": true, "draggable": true, "x": 178, "y": 140, "textAlign": "center", "fontWeight":"bold","font": "Josefin Sans", "colors": ["#891C2F"], "textSize": 20}},{"type": "text", "params": {"src": "Symbolize ", "editorMode": true, "draggable": true, "x": 282, "y": 138, "textAlign": "center", "fontWeight":"bold","font": "Dancing Script", "colors": ["#891C2F"], "textSize": 20}},{"type": "text", "params": {"src": "New Beginnings ", "editorMode": true, "draggable": true, "x": 179, "y": 160, "textAlign": "center", "fontWeight":"bold","font": "Dancing Script ", "colors": ["#891C2F"], "textSize": 25}},{"type": "text", "params": {"src": "~and~ ", "editorMode": true, "draggable": true, "x": 175, "y": 184, "textAlign": "center", "fontWeight":"bold","font": "Dancing Script ", "colors": ["#891C2F"], "textSize": 20}},{"type": "text", "params": {"src": "Eternal Life ", "editorMode": true, "draggable": true, "x": 178, "y": 201, "textAlign": "center", "fontWeight":"bold","font": "Dancing Script ", "colors": ["#891C2F"], "textSize": 35}},{"type": "image", "params": {"src": "assets/img/sample/wings.png","editorMode": true, "draggable": true, "resizeToW": 180, "x": 282, "y": 166,"degree":63}},{"type": "image", "params": {"src": "assets/img/sample/peacock.png","editorMode": true, "draggable": true, "resizeToW": 353, "x": 290, "y": 124,"degree":34}}]}'>
												</div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-01/right-preview.png", "base": "assets/img/products/mug-01/right-base.png", "template": [{"type": "image", "params": {"src": "assets/img/sample/wings.png","editorMode": true, "draggable": true, "resizeToW": 150, "x": 70, "y": 119}},{"type": "image", "params": {"src": "assets/img/sample/wings.png","editorMode": true, "draggable": true, "resizeToW": 180, "x": 86, "y": 166}},{"type": "image", "params": {"src": "assets/img/sample/wings.png","editorMode": true, "draggable": true, "resizeToW": 217, "x": 137, "y": 198}}]}'>
												</div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-02/mug-thumbnail.png" alt="mug 2" title="mug">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-02/left-preview.png", "base": "assets/img/products/mug-02/left-base.png","template": [{"type": "image", "params": {"src": "assets/img/sample/flowers3.png","editorMode": true, "draggable": true, "resizeToW": 262, "x": 155, "y": 128}}]}'>
												</div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-02/right-preview.png", "base": "assets/img/products/mug-02/right-base.png", "template": [{"type": "image", "params": {"src": "assets/img/sample/butterfly1.png","editorMode": true, "draggable": true, "resizeToW": 176, "x": 130, "y": 145}},{"type": "image", "params": {"src": "assets/img/sample/butterfly2.png","editorMode": true, "draggable": true, "resizeToW": 109, "x": 93, "y": 170}}]}'>
												</div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-03/mug-thumbnail.png" alt="mug" title="mug">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-03/left-preview.png", "base": "assets/img/products/mug-03/left-base.png","template": [{"type": "text", "params": {"src": "Tea ", "editorMode": true, "draggable": true, "x": 261, "y": 170,"textAlign": "center", "fontWeight":"bold","font": "Dancing Script", "colors": ["#891C2F"], "textSize": 30}},{"type": "text", "params": {"src": "Appreciation ", "editorMode": true, "draggable": true, "x": 215, "y": 196, "textAlign": "center", "fontWeight":"bold","font": "Dancing Script", "colors": ["#891C2F"], "textSize": 28}},{"type": "text", "params": {"src": "Society ", "editorMode": true, "draggable": true, "x": 246, "y": 227, "textAlign": "center", "fontWeight":"bold","font": "Dancing Script", "colors": ["#891C2F"], "textSize": 30}},{"type": "image", "params": {"src": "assets/img/sample/coffee.png","editorMode": true, "draggable": true, "resizeToW": 128, "x": 217, "y": 246}},{"type": "image", "params": {"src": "assets/img/sample/stars.png","editorMode": true, "draggable": true, "resizeToW": 225, "x": 177, "y": 139}}]}'>
												</div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-03/right-preview.png", "base": "assets/img/products/mug-03/right-base.png", "template": [{"type": "image", "params": {"src": "assets/img/sample/cat.png","editorMode": true, "draggable": true, "resizeToW": 119, "x": 145, "y": 134}},{"type": "text", "params": {"src": "AD ", "editorMode": true, "draggable": true, "x": 120, "y": 218,"textAlign": "center", "fontWeight":"bold","font": "Quicksand", "colors": ["#D1316F"], "textSize": 115}},{"type": "text", "params": {"src": "Stephanic ", "editorMode": true, "draggable": true, "x": 134, "y": 323,"textAlign": "center", "fontWeight":"bold","font": "Play", "colors": ["#D1316F"], "textSize": 25,"fontWeight":"bold"}},{"type": "text", "params": {"src": "CLASS OF 20XX ", "editorMode": true, "draggable": true, "x": 284, "y": 349,"textAlign": "center", "fontWeight":"bold","font": "Play", "colors": ["#D1316F"], "textSize": 25,"fontWeight":"bold","degree":270}}]}'>
												</div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-04/mug-thumbnail.png" alt="mug" title="mug">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-04/left-preview.png", "base": "assets/img/products/mug-04/left-base.png","template": [{"type": "text", "params": {"src": "Seasons", "editorMode": true, "draggable": true, "x": 288, "y": 179, "textAlign": "center", "font": "Dancing Script", "colors": ["#497BBA"], "textSize": 40,"fontWeight":"bold"}},{"type": "text", "params": {"src": "Greetings", "editorMode": true, "draggable": true, "x": 282, "y": 222, "textAlign": "center", "font": "Dancing Script", "colors": ["#497BBA"], "textSize": 35,"fontWeight":"bold"}},{"type": "image", "params": {"src": "assets/img/sample/clouds.png","editorMode": true, "draggable": true, "resizeToW": 92, "x": 93, "y": 170}},{"type": "image", "params": {"src": "assets/img/sample/clouds.png","editorMode": true, "draggable": true, "resizeToW": 94, "x": 173, "y": 171}},{"type": "image", "params": {"src": "assets/img/sample/clouds.png","editorMode": true, "draggable": true, "resizeToW": 93, "x": 111, "y": 222}},{"type": "image", "params": {"src": "assets/img/sample/clouds.png","editorMode": true, "draggable": true, "resizeToW": 98, "x": 188, "y": 220}},{"type": "image", "params": {"src": "assets/img/sample/dots.png","editorMode": true, "draggable": true, "resizeToW": 255, "x": 150, "y": 289}}]}'>
												</div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-04/right-preview.png", "base": "assets/img/products/mug-04/right-base.png", "template": [{"type": "image", "params": {"src": "assets/img/sample/dots1.png","editorMode": true, "draggable": true, "resizeToW": 72, "x": 52, "y": 154}},{"type": "image", "params": {"src": "assets/img/sample/dots1.png","editorMode": true, "draggable": true, "resizeToW": 69, "x": 130, "y": 163}},{"type": "image", "params": {"src": "assets/img/sample/dots1.png","editorMode": true, "draggable": true, "resizeToW": 67, "x": 206, "y": 165}},{"type": "image", "params": {"src": "assets/img/sample/dots1.png","editorMode": true, "draggable": true, "resizeToW": 64, "x": 279, "y": 162}},{"type": "image", "params": {"src": "assets/img/sample/dots1.png","editorMode": true, "draggable": true, "resizeToW": 61, "x": 347, "y": 152}},{"type": "image", "params": {"src": "assets/img/sample/dots.png","editorMode": true, "draggable": true, "resizeToW": 316, "x": 73, "y": 226}},{"type": "image", "params": {"src": "assets/img/sample/dots1.png","editorMode": true, "draggable": true, "resizeToW": 37, "x": 129, "y": 324}},{"type": "image", "params": {"src": "assets/img/sample/dots1.png","editorMode": true, "draggable": true, "resizeToW": 47, "x": 172, "y": 335}},{"type": "image", "params": {"src": "assets/img/sample/dots1.png","editorMode": true, "draggable": true, "resizeToW": 71, "x": 227, "y": 310}},{"type": "image", "params": {"src": "assets/img/sample/dots1.png","editorMode": true, "draggable": true, "resizeToW": 71, "x": 291, "y": 267}}]}'>
												</div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-05/mug-thumbnail.png" alt="mug" title="mug">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-05/left-preview.png", "base": "assets/img/products/mug-05/left-base.png","template": [{"type": "image", "params": {"src": "assets/img/sample/Flowers.png","editorMode": true, "draggable": true, "resizeToW": 201, "x": 88, "y": 135,"degree":9}},{"type": "image", "params": {"src": "assets/img/sample/Flowers.png","editorMode": true, "draggable": true, "resizeToW": 205, "x": 263, "y": 171,"degree":349}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 113, "x": 111, "y": 236}},{"type": "image", "params": {"src": "assets/img/sample/doll.png","editorMode": true, "draggable": true, "resizeToW": 142, "x": 181, "y": 237}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 121, "x": 304, "y": 239}}]}'>
												</div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-05/right-preview.png", "base": "assets/img/products/mug-05/right-base.png", "template": [{"type": "image", "params": {"src": "assets/img/sample/teddy.png","editorMode": true, "draggable": true, "resizeToW": 266, "x": 114, "y": 157}}]}'>
												</div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->				
									</div>
								</div>
								<!--/ Products -->

								<!-- Product Options Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-product-options">
									<p class="custom-pane-header text-center h3">Product Options</p><hr>
									<div class="row display-gallery base-colors">
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#A2C44C" style="background-color: #A2C44C"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#28272c" style="background-color: #28272c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#e975a4" style="background-color: #e975a4"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#891c2f" style="background-color: #891c2f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#6e95ce" style="background-color: #6e95ce"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#b3042b" style="background-color: #b3042b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#7d5b52" style="background-color: #7d5b52"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#fed348" style="background-color: #fed348"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#3c3230" style="background-color: #3c3230"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#333a32" style="background-color: #333a32"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#feac1c" style="background-color: #feac1c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#d1316f" style="background-color: #d1316f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#edb963" style="background-color: #edb963"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#c2bebf" style="background-color: #c2bebf"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#4a5d7b" style="background-color: #4a5d7b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#497bba" style="background-color: #497bba"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#00995b" style="background-color: #00995b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#01858a" style="background-color: #01858a"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#008767" style="background-color: #008767"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#85a35d" style="background-color: #85a35d"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#9cbad6" style="background-color: #9cbad6"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#f0c6dc" style="background-color: #f0c6dc"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#83cb67" style="background-color: #83cb67"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#612b39" style="background-color: #612b39"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#FFFFFF" style="background-color: #FFFFFF"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#304972" style="background-color: #304972"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#5a5947" style="background-color: #5a5947"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#e7dbcb" style="background-color: #e7dbcb"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#304050" style="background-color: #304050"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#282b3c" style="background-color: #282b3c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#564d46" style="background-color: #564d46"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#FF6633" style="background-color: #FF6633"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#b8a8cd" style="background-color: #b8a8cd"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#C8D8AB" style="background-color: #C8D8AB"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#C48710" style="background-color: #C48710"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#663D50" style="background-color: #663D50"></div></div>
									</div>
								</div>
								<!--/ Product Options Pane -->
								
								<!-- Text Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-text-options">
									<p class="custom-pane-header text-center h3">Text Options</p><hr>
									<button id="btn-add-text" type="button" class="btn btn-default btn-lg btn-block">Add Text</button>
									<br/>

									<select class="form-control input-lg" id="text-font-family">
										<!-- <option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
										<option style="font-family:Courier New" value="Courier New">Courier New</option>
										<option style="font-family:Comic Sans MS" value="Comic Sans MS">Comic Sans MS</option>
										<option style="font-family:Times New Roman" value="Times New Roman">Times New Roman</option>
										<option style="font-family:Palatino Linotype" value="Palatino Linotype">Palatino Linotype</option>
										<option style="font-family:Impact" value="Impact">Impact</option>
										<option style="font-family:Arial" value="Arial">Arial</option>
										<option style="font-family:Tahoma" value="Tahoma">Tahoma</option>
										<option style="font-family:Arial Black" value="Arial Black">Arial Black</option>
										<option style="font-family:Verdana" value="Verdana">Verdana</option>
										<option style="font-family:MS Sans Serif" value="MS Sans Serif">MS Sans Serif</option> -->
										
										<option style="font-family:Arial" value="Arial">Arial</option>
										<option style="font-family:Indie Flower" value="Indie Flower">Indie Flower</option>
										<option style="font-family:Poiret One" value="Poiret One">Poiret One</option>
										<option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
										<option style="font-family:Shadows Into Light" value="Shadows Into Light">Shadows Into Light</option>
										<option style="font-family:Pacifico" value="Pacifico">Pacifico</option>
										<option style="font-family:Quicksand" value="Quicksand">Quicksand</option>
										<option style="font-family:Architects Daughter" value="Architects Daughter">Architects Daughter</option>
										<option style="font-family:Dancing Script" value="Dancing Script">Dancing Script</option>
										<option style="font-family:Chewy" value="Chewy">Chewy</option>
										<option style="font-family:Gloria Hallelujah" value="Gloria Hallelujah">Gloria Hallelujah</option>

									</select>
									<br>
									<div class="btn-lg color-selector">
										<div class="pull-left">Color</div>
										<div class="pull-right">
											<div class="color-preview"></div>
										</div>
										<div class="clearfix"></div>
									</div>									
									<div class="display-gallery hidden" id="text-font-fill">
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
									</div>
									<div class="clearfix"></div>
									<br>
									
									<div class="btn-group btn-group-justified" role="group" id="text-properties">
										<div class="btn-group" role="group">
											<button type="button" id="text-bold" class="btn btn-default"><i class="fa fa-bold"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="text-italic" class="btn btn-default"><i class="fa fa-italic"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="text-underline" class="btn btn-default"><i class="fa fa-underline"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="text-align-left" class="btn btn-default"><i class="fa fa-align-left"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="text-align-center" class="btn btn-default"><i class="fa fa-align-center"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="text-align-right"  class="btn btn-default"><i class="fa fa-align-right"></i></button>
										</div>
									</div>
								</div>
								<!--/ Text Pane -->
								
								<!-- Image Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-image-options">
									<p class="custom-pane-header text-center h3">Image Options</p><hr>
									<div class="btn-lg color-selector">
										<div class="pull-left">Color</div>
										<div class="pull-right">
											<div class="color-preview"></div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="display-gallery hidden" id="image-fill">
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#000000" style="background-color: #000000"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
									</div>
									<div class="clearfix"></div>
									<br>
									<ul class="nav nav-tabs" role="tablist">
										<li class="active"><a role="presentation" href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
										<li><a role="presentation" href="#uploads" aria-controls="uploads" role="tab" data-toggle="tab">Uploads</a></li>
									</ul>

									<div class="tab-content">
										<div role="tabpanel" class="tab-pane fade in active" id="gallery">
											<div class="row display-gallery">
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/nike.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/adidas.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/being-human.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/puma.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/tattoos-01.png" alt=""></div>
												
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/camera-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/dragon-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/hearts-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/happy-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/sad-01.png" alt=""></div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="uploads">
											<div class="row display-gallery">
												<div class="col-sm-24 text-center">
													<br>
													<button id="btn-add-img" type="button" class="btn btn-default btn-lg"><i class="ion-ios-upload-outline"></i> Upload Image</button>
													<input type="file" id="add-img" style="display: none;" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Image Pane -->

								<!-- Cart Calculations Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-price-calculation-options">
										<p class="custom-pane-header text-center h3">Price Calculations</p><hr>
										<div class="row">
											<div class="col-xs-12 col-sm-16 col-md-24">
												<!-- table -->
												<table class="table table-hover">
													<thead>
														<tr>
															<th>Size</th>
															<th class="text-center">#</th>
															<th class="text-center">Total</th>
														</tr>
													</thead>
													<tbody>
														<tr data-toggle="popover" data-popover-size="xs" title="Name & Number" data-html="true" data-content="<div data-nn-size='xs'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>XS</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="xs" class="sizes xs_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="xs_total">0</span></td>
														</tr>
														<tr data-toggle="popover" data-popover-size="s" title="Name & Number" data-html="true" data-content="<div data-nn-size='s'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>S</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="s" class="sizes s_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="s_total">0</span></td>
														</tr>
														<tr data-toggle="popover" data-popover-size="m" title="Name & Number" data-html="true" data-content="<div data-nn-size='m'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>M</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="m" class="sizes m_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="m_total">0</span></td>
														</tr>												
														<tr data-toggle="popover" data-popover-size="l" title="Name & Number" data-html="true" data-content="<div data-nn-size='l'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>L</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="l" class="sizes l_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="l_total">0</span></td>
														</tr>
														<tr data-toggle="popover" data-popover-size="xl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>XL</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="xl" class="sizes xl_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="xl_total">0</span></td>
														</tr>
														<tr data-toggle="popover" data-popover-size="xxl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xxl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>XXL</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="xxl" class="sizes xxl_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="xxl_total">0</span></td>
														</tr>
														<tr>
															<td></td>													
															<td class="text-center">
																<h4><strong>Total</strong></h4>
															</td>
															<td class="text-center text-danger">
																<h4><strong>$<span class="total_price">0.00</span></strong></h4>	
															</td>
														</tr>
													</tbody>
												</table>
												<!--/ table -->
												<button class="btn btn-lg btn-block btn-default">Add To Cart</button>
											</div>
										</div>
								</div>
								<!--/ Cart Calculations Pane -->
							</div>
							<!--/ Tab Panes -->
							<br>
						</div>
					</div>
					<!--/ Left -->
					
					<div class="col-sm-14 col-md-14">
						<div class="row">
							<!-- Center -->
							<div class="col-sm-14 col-md-18">
								<!-- Design Tool Stage-->
								<div class="col-xs-24">
									<div class="text-center stage" id="design-studio">
										<div data-side="front" data-params='{"preview": "assets/img/products/mug-05/left-preview.png", "base": "assets/img/products/mug-05/left-base.png","template": [{"type": "image", "params": {"src": "assets/img/sample/Flowers.png","editorMode": true, "draggable": true, "resizeToW": 201, "x": 88, "y": 135,"degree":9}},{"type": "image", "params": {"src": "assets/img/sample/Flowers.png","editorMode": true, "draggable": true, "resizeToW": 205, "x": 263, "y": 171,"degree":349}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 113, "x": 111, "y": 236}},{"type": "image", "params": {"src": "assets/img/sample/doll.png","editorMode": true, "draggable": true, "resizeToW": 142, "x": 181, "y": 237}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 121, "x": 304, "y": 239}}]}'>
										</div>
										<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-05/right-preview.png", "base": "assets/img/products/mug-05/right-base.png", "template": [{"type": "image", "params": {"src": "assets/img/sample/teddy.png","editorMode": true, "draggable": true, "resizeToW": 266, "x": 114, "y": 157}}]}'>
										</div>
									</div>
								</div>
								<!--/ Design Tool Stage-->
							</div>
							<!--/ Center -->

							<!-- Right -->
							<div class="col-sm-10 col-md-6">
								<!-- Preview Images -->
								<div class="col-sm-12 text-center preview-images">
									<div class="row">
										<div class="col-xs-6 col-sm-24">
											<img src="" class="img-thumbnail" id="front-preview" alt="Front">
										</div>
										<div class="col-xs-6 col-sm-24">
											<img src="" class="img-thumbnail" id="back-preview" alt="Back">
										</div>
									</div>							
								</div>
								<!--/ Preview Images -->
							</div>
							<!--/ Right -->
						</div>
						<div class="row">
							<!-- Price -->
							<div class="col-sm-10 col-sm-offset-14 col-md-6 col-md-offset-18">
								<div class="col-sm-12 text-center total_price_col">
									<br>
									<div class="well well-sm"><h4>$<span class="total_price">0.00</span></h4></div>
								</div>
							</div>
							<!--/ Price -->
						</div>
					</div>
				</div>
				<!--/ Design Tool -->

				<!-- Price -->
				<!-- <div class="row">
					<div class="col-sm-6 col-md-4 col-sm-offset-18 col-md-offset-20">
						<span class="visible-xs"><br><br><br><br><br><br></span>
						<div class="col-sm-12 text-center total_price_col">
							<br>
							<div class="well well-sm"><h4>$<span class="total_price">0.00</span></h4></div>
						</div>
					</div>
				</div> -->
				<!--/ Price -->
			</div>
		</div>
		<!--/  Main Container -->
		<!-- Pre-load Fonts -->
		<section class="preload-fonts"></section>
		<!--/ Pre-load Fonts -->
		<!-- JS -->
		<!-- plugins -->
		<script type="text/javascript" src="assets/lib/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/lib/confirmation/bootstrap-confirmation.min.js"></script>
		<script type="text/javascript" src="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="assets/lib/fabric/fabric.min.js"></script>		
		<script type="text/javascript" src="assets/lib/design-studio/design-studio.js"></script>
		<!--/ plugins -->
		<script type="text/javascript" src="assets/js/index.js"></script>
		<!--/ JS -->
	</body>
</html>