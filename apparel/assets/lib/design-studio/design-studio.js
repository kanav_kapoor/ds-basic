/*
 * Design Studio Basic - Copyrights Browsewire 2015
 *
 */

(function($) {
	var designStudio = function(container, customOptions){

		// Load default options and replace it with customized options
		var options = $.extend({}, $.fn.designStudio.defaultOptions, customOptions);
		options.dimensions = $.extend({}, $.fn.designStudio.defaultOptions.dimensions, options.dimensions);

		// Define global variables
		var designStudio = this,
				canvas,
				stage,
				currentSide,
				currentElement,
				template,
				params,
				fabricParams,
				$productContainer;

		/* Private Methods Started */
		var _testCanvas = function(){
			var canvasTest = document.createElement('canvas');

			if(!Boolean(canvasTest.getContext && canvasTest.getContext('2d'))){
				alert("Your browser doesn\'t support custom design. Please use a different browser");
				return;
			}
		}

		var _modifyFabricDefaultPrototype = function(){
			// Changing Default Fabric Prototype
			var _original = fabric.Object.prototype._drawControl;
			fabric.Object.prototype._drawControl = function(control, ctx, methodName, left, top) {
				var size = this.cornerSize;
				if (this.canvas.hasControlCallback && this.canvas.hasControlCallback[control]) {
					this.canvas.controlCallback[control](ctx, left, top, size);
				} else {
					_original.call(this, control, ctx, methodName, left, top);
				}
			};

			fabric.Canvas.prototype.cursorMap = [
				'pointer',
				'pointer',
				'pointer',
				'pointer',
				'pointer',
				'pointer',
				'pointer',
				'pointer'
			];
		}

		var _centerObject = function(element){
			element.center();
			element.setCoords();
		}

		var _doModification = function(element){
			var currentElementParams = element.params;

			if(	typeof currentElementParams.colors === 'object' ||
				currentElementParams.removable ||
				currentElementParams.draggable ||
				currentElementParams.resizable ||
				currentElementParams.rotatable ||
				currentElementParams.zChangeable ||
				currentElementParams.editable ||
				currentElementParams.patternable
				|| currentElementParams.uploadZone) {

				element.set('selectable', true);

				if(currentElementParams.draggable) {
					element.lockMovementX = element.lockMovementY = false;
				}

				if(currentElementParams.rotatable) {
					element.lockRotation = false;
				}

				if(currentElementParams.resizable) {
					element.lockScalingX = element.lockScalingY = false;
				}

				if((currentElementParams.resizable || currentElementParams.rotatable || currentElementParams.removable)) {
					element.hasControls = true;
				}
			}
		}

		var _fabricElemCreated = function(element){
			//dd(stage);
			
			//dd(element);
			params = element.params;

			// If side is defined, add element to that side
			if(typeof params.side != "undefined") {
				stage = window[params.side];
			}

			stage.add(element);
			//dd(stage.getObjects());

			// If is base/preview, save in global scope and resize it
			if(params.base || params.preview) {
				// It is base image
				if(typeof params.base != "undefined")
					window[params.side+"Base"] = element;
				else
				// It is preview image
					window[params.side+"Preview"] = element;
				//console.log("Stage Dimensions - "+stage.getWidth()+" - "+stage.getHeight());
				//console.log("Element Dimensions - "+element.width+" - "+element.height);
				
				element.scaleToWidth(stage.getWidth());
				
			
				if(element.getHeight() > stage.getHeight()){
					element.scaleToHeight(stage.getHeight());
					//element.scaleToWidth(stage.getWidth());
					//console.log("Element Dimensions - "+element.width+" - "+element.height);
				}
				_centerObject(element);
			}

			if(params.base) {
				element.set({selectable: true, evented: true});
			}

			
			_doModification(element);
			// Scale object to canvas width
			if(params.scaleToWidth) {
				element.scaleToWidth(stage.getWidth());
			}

			if(params.resizeToW){
				element.scaleToWidth(params.resizeToW);
			}

			if(params.autoCenter)
				_centerObject(element);

			if(params.autoSelect){
				stage.setActiveObject(element);
				element.setCoords();
			}
			
			if(params.sendToBack)
				stage.sendToBack(element);
			
			stage.renderAll().calcOffset();

			// Reset stage
			if(typeof params.resetStageTo != "undefined") stage = params.resetStageTo;
		}

		//Toggle Object Property
		var toggleObjPropCore = function(element, conditionType, property, options){
			if(element.type === conditionType){
				var newObj = {};
				newObj[property] = (element[property] == options[1] ? options[0] : options[1]);
				element.set(newObj);
				element.params[property] = newObj[property];
				stage.renderAll();
			}
		}

		var _toggleObjProp = function(element, conditionType, property, options){	
			//dd(element, conditionType, property, options);
			if(conditionType.constructor === Array){
				$.each(conditionType, function(index, value){
					toggleObjPropCore(element, value, property, options);
				});
			}else{
				toggleObjPropCore(element, conditionType, property, options);
			}
		}

		var _setStyle = function(object, property, value){
			object[property] = object.params[property] = value;
		}

		var updateColorDisplayDiv = function (element, color){
			element.css("background-color", color);
		}

		var _preloadImages = function(images){
			$.each(images, function(i, img){
				$("body").append("<img src='"+img+"' class='hide'>");
			});
		}

		var _changeImageColor = function(obj, color){
			obj.filters = [];
			obj.filters.push(new fabric.Image.filters.Tint({color: color, opacity: 1}));
			try {				
				obj.applyFilters(stage.renderAll.bind(stage));
			}
			catch(evt) {
				alert("Color cannot be changed. Please try again");
			}
		}

		var _resizeDisplay = function($productContainer){
			var productStageWidth = options.dimensions.productStageWidth;
			var productStageHeight = options.dimensions.productStageHeight;
			var productDisplayWidth = options.dimensions.productDisplayWidth;

			var widthDiff = productStageWidth-productDisplayWidth;
			var widthDiffProportionately = widthDiff/productStageWidth;
			var productDisplayHeight = productStageHeight - (productStageHeight*widthDiffProportionately);

			$productContainer.find(".upper-canvas, .lower-canvas, .canvas-container").width(productDisplayWidth).height(productDisplayHeight);
			$productContainer.find(".canvas-container").css({border: '3px dashed #DDD', overflow: 'hidden'});
		}

		var _setDimensions = function(side, $productContainer){
			side.setDimensions({width: options.dimensions.productStageWidth, height: options.dimensions.productStageHeight});
			_resizeDisplay($productContainer);
		}
		

		var _changeCanvasIcons = function(side){
			side.hasControlCallback = {
				tr: true,
				mtr: true
			};

			side.controlCallback = {
				tr: function (ctx, left, top, size) {
					var image = new Image(), x, y;

					image.src = 'assets/img/control-icons/delete.png';
					x = left - image.width/2 + size/2;
					y = top - image.height/2 + size/2;

					ctx.drawImage(image, x, y);
				},
				mtr: function (ctx, left, top, size) {
					var image = new Image(), x, y;

					image.src = 'assets/img/control-icons/rotate.png';
					x = left - image.width/2 + size/2;
					y = top - image.height/2 + size/2;

					ctx.drawImage(image, x, y);
				}
			};
		}

		var _resetTextTab = function(){
			$("#text-font-family").val($("#text-font-family option:first").val());
			$("#text-properties button").removeClass("active");
		}

		var _updateTextTab = function(element){
			var fill           = element.fill;
			var fontFamily     = element.fontFamily;
			var fontWeight     = element.fontWeight;
			var fontStyle      = element.fontStyle;
			var textDecoration = element.textDecoration;
			var textAlign      = element.textAlign;
			var lineHeight     = element.lineHeight;
			var opacity        = element.opacity;

			_resetTextTab();

			if(element.fontFamily)
				$("#text-font-family").val(element.fontFamily);

			if(fontWeight)
				$("#text-bold").addClass("active");

			if(fontStyle)
				$("#text-italic").addClass("active");

			if(textDecoration)
				$("#text-underline").addClass("active");

			$("#text-align-"+textAlign).addClass("active");
		}

		var _showTab = function(elementType){
			if (elementType == "text" || elementType == "i-text" || elementType == "curvedText") {
				$("li a[href='#tabpane-text-options']").tab('show');
			}else if (elementType == "image") {
				$("li a[href='#tabpane-image-options']").tab('show');
			}else if (elementType == "product") {
				$("li a[href='#tabpane-product-options']").tab('show');
			}
		}

		var _updateTabs = function(element){
			var elementType = typeof currentElement.params.base != "undefined" ? "product" : currentElement.type;
			_showTab(elementType);
			/*var elementType = currentElement.type;
			if (elementType == "text" || elementType == "i-text") {
				_updateTextTab(currentElement);
			};*/
		}

		var _registerEvents = function(side){
			side.on('object:selected', function(opts){
				currentElement = opts.target;

				currentElement.set({
					borderColor: 'red',
					cornerColor: 'red',
					cornerSize: 16,
				});

				if(currentElement.params.base){
					currentElement.hasBorders = currentElement.hasControls = false;
				}

				_updateTabs(currentElement);
				
			})

			side.on('object:added', function(opts){
				setTimeout(function(){designStudio.setPreviewImages()}, 200);
			})

			side.on('object:removed', function(opts){
				setTimeout(function(){designStudio.setPreviewImages()}, 200);
			})

			side.on('object:modified', function(opts){
				//console.log("modified");
				//currentElement = opts.target;
				//_updateTabs(currentElement);
				setTimeout(function(){designStudio.setPreviewImages()}, 200);
			})

			

			side.on('mouse:up', function(opts) {
				if (opts.target) {
					if(opts.target == undefined) {
						designStudio.deselectElement();
					}
					else {
						var targetCorner = opts.target.__corner;
						var currentElement = stage.getActiveObject();
						var type = currentElement.type;


						//remove element
						if(targetCorner == 'tr'){
							currentElement.remove();
							if(type == "text" || type == "i-text"){
								designStudio.modifyAddons("minus", "text");
							}else if(type == "image"){
								designStudio.modifyAddons("minus", "image");
							}
							designStudio.updatePrice();
						}
					}
				}
			});

			// Keep Objects Within Limit
			side.on('object:moving', function (e) {
				var obj = e.target;
				 // if object is too big ignore
				if(obj.currentHeight > obj.canvas.height || obj.currentWidth > obj.canvas.width){
						return;
				}        
				obj.setCoords();        
				// top-left  corner
				if(obj.getBoundingRect().top < 0 || obj.getBoundingRect().left < 0){
						obj.top = Math.max(obj.top, obj.top-obj.getBoundingRect().top);
						obj.left = Math.max(obj.left, obj.left-obj.getBoundingRect().left);
				}
				// bot-right corner
				if(obj.getBoundingRect().top+obj.getBoundingRect().height  > obj.canvas.height || obj.getBoundingRect().left+obj.getBoundingRect().width  > obj.canvas.width){
						obj.top = Math.min(obj.top, obj.canvas.height-obj.getBoundingRect().height+obj.top-obj.getBoundingRect().top);
						obj.left = Math.min(obj.left, obj.canvas.width-obj.getBoundingRect().width+obj.left-obj.getBoundingRect().left);
				}
			});
		}

		var _registerHandlers = function(){
			$(window).resize(function(){
				options.dimensions.productDisplayWidth = $(container).children('div:not(".hide")').width();

				$(container).children('div').each(function(){
					_resizeDisplay($(this));
				});
			});
		}

		var _addDefaultParams = function(defaultParams, currentSide){
			if(typeof defaultParams.preview != "undefined")
					designStudio.addElement("image", defaultParams.preview, {preview: true, side: currentSide, resetStageTo: front, sendToBack: true});
				
			designStudio.addElement("image", defaultParams.base, {base: true, side: currentSide, resetStageTo: front});

			// Add Template
			if(typeof defaultParams.template != "undefined"){
				setTimeout(function(){
					$.each(defaultParams.template, function(i, obj){
						var params = obj.params;
						if(obj.type === "text"){
							designStudio.addText(params.src, $.extend({side: currentSide, resetStageTo: front}, params));
						}else if(obj.type === "image"){
							designStudio.addImage(params.src, $.extend({side: currentSide, resetStageTo: front}, params));
						}
					});
				}, 1000);
			}
		}

		var _init = function(){
			_testCanvas();
			_modifyFabricDefaultPrototype();

			$(".preview-images img").parent().hide();

			// empty sides array
			$.fn.designStudio.defaultOptions.sides = [];
			designStudio.resetPrice();
			$(container).children('div:lt(2)').each(function(){
				$productContainer = $(this);

				// HTML Manipulations				
				$productContainer.append('<canvas></canvas>');
				canvas = $productContainer.children('canvas').get(0);

				var currentSide = $productContainer.data('side');
				$.fn.designStudio.defaultOptions.sides.push(currentSide);

				// Use Fabric to set the stage
				window[currentSide] = new fabric.Canvas(canvas, {
					selection: false,
					hoverCursor: 'pointer',
					rotationCursor: 'default',
					controlsAboveOverlay: true,
					centeredScaling: true
				});

				_setDimensions(window[currentSide], $productContainer);
				_changeCanvasIcons(window[currentSide]);
				_registerEvents(window[currentSide], $productContainer);

				
				_addDefaultParams($productContainer.data('params'), currentSide);
				$("#"+currentSide+"-preview").parent().show();				
			})

			_registerHandlers();
			_preloadImages(['assets/img/control-icons/delete.png', 'assets/img/control-icons/rotate.png']);
			$("#loader").addClass("hidden");

		}


		setTimeout(_init, 2000);

		
		/* Private Methods Ended */


		/* Public Methods Stared */
		var changeStage = function(side){
			stage = window[side];
			currentSide = side;
		}


		designStudio.changeSide = function(side){
			// Change stage
			changeStage(side);
			
			$(container).children("div").each(function(){
				if($(this).data('side') === side)
					$(this).removeClass('hide');
				else
					$(this).addClass('hide');
			});
		}

		designStudio.addElement = function(type, source, params){
			var addElementDeferred = $.Deferred();
			var params = $.extend({}, options.elementParameters, params);
			
			//return;
			if(type == "text"){
				params = $.extend({}, options.textParameters, params);				
			}

			var fabricParams = {
				source: source,
				//title: title,
				top: params.y,
				left: params.x,
				//originX: params.originX,
				//originY: params.originY,
				scaleX: params.scale,
				scaleY: params.scale,
				angle: params.degree,
				opacity: params.opacity,
				id: String(new Date().getTime()),
				//visible: containerIndex == currentViewIndex,
				//viewIndex: containerIndex,
				lockUniScaling: true,
				lineHeight: 1.2
			};

			if(params.editorMode) {
				params.removable = params.resizable = params.rotatable = params.zChangeable = true;
			}
			else {
				$.extend(fabricParams, {
					selectable: false,
					lockRotation: true,
					lockScalingX: true,
					lockScalingY: true,
					lockMovementX: true,
					lockMovementY: true,
					hasControls: false,
					evented: false
				});
			}

			if(type == "image"){
				var imageLoaded = function(fabricImage, params){
					$.extend(fabricParams, {params: params, originParams: $.extend({}, params)});
					fabricImage.set(fabricParams);
					
					//dd(fabricImage.params);
					if(fabricImage.getWidth() < options.customImagesParameters.minW){
						alert("PLease upload am image with resolution more than 20px");
						return;
					}

					if(fabricImage.getWidth() > options.customImagesParameters.maxW){
						fabricImage.scaleToWidth(options.customImagesParameters.resizeToW);
					}

					if(fabricImage.getHeight() > options.customImagesParameters.maxH){
						fabricImage.scaleToHeight(options.customImagesParameters.resizeToH);								
					}

					//if(params.logo) logo = fabricImage;
					_fabricElemCreated(fabricImage);
					addElementDeferred.resolve()
				}


				fabric.Image.fromURL(source, function(fabricImage) {
					imageLoaded(fabricImage, params);
				});
				
				/*var imgSplitted = source.split('.');
				// Base64
				if(imgSplitted.length == 1) {
				}else {
					fabric.Image.fromURL(source, function(fabricImage) {
						console.log(fabricImage);
					});
				}*/



				
			}else if(type == "text"){
				params.text = params.text ? params.text : params.source;
				
				//params.font = params.font ? params.font : options.fonts[0];
				if(params.font == undefined) {
					params.font = 'Arial';
				}

				$.extend(fabricParams, {
					fontSize: params.textSize,
					fontFamily: params.font,
					fontStyle: params.fontStyle,
					fontWeight: params.fontWeight,
					textAlign: params.textAlign,
					textBackgroundColor: params.textBackgroundColor,
					lineHeight: params.lineHeight,
					textDecoration: params.textDecoration,
					fill: params.colors[0] ? params.colors[0] : "#000000",
					editable: params.editable,
					spacing: params.curveSpacing,
					radius: params.curveRadius,
					reverse: params.curveReverse,
					params: params,
					originParams: $.extend({}, params)
				});


				var fabricText = new fabric.IText(params.text.replace(/\\n/g, '\n'), fabricParams);
				
				_fabricElemCreated(fabricText);
			}

			return addElementDeferred.promise();
		}

		function deleteObjectKey(object, key){
			if(typeof object[key] != "undefined") delete object[key]; 
		}

		function objectKeyCount(o){
			var count = 0;
			for(var i in o){
				if(o.hasOwnProperty(i)) count++;
			}
			return count;
		}

		designStudio.modifyAddons = function(type, elementType, params){
			if(elementType == "text" || elementType == "image"){
				if(type == "add") $.fn.designStudio.defaultOptions.addons[elementType] += 1;
				else if(type == "minus") $.fn.designStudio.defaultOptions.addons[elementType] -= 1;
			}else if(elementType == "productFill" || elementType == "productTexture" || elementType == "productPattern") {
				
				if(type == "add"){
					var object = {};
					object.src = params.src;
					var addons = $.fn.designStudio.defaultOptions.addons;
					
					deleteObjectKey(addons.productFill, params.name);
					deleteObjectKey(addons.productTexture, params.name);
					deleteObjectKey(addons.productPattern, params.name);

					addons[elementType][params.name] = object; 
				}
			}else if(type == "minus"){
					deleteObjectKey(addons[elementType], params.name);
			}

			//dd($.fn.designStudio.defaultOptions.addons);
			//dd(designStudio.calculatePrice());
		}

		designStudio.calculatePrice = function(){

			/*{
				"text": 1,
				"image": 1,
				"productColor": {base: {src: ''}},
				"productTexture": {base: {src: ''}},
				"productPattern": {base: {src: ''}}
			}*/
			var priceObject = {};
			priceObject.base = {count: 1, total: $.fn.designStudio.defaultOptions.price.base};
			var addons = $.fn.designStudio.defaultOptions.addons;
			
			$.each(addons, function(type, details){
				priceObject[type] = {};
				var price = $.fn.designStudio.defaultOptions.price[type];
				
				// text, image
				if(typeof details === "number"){
					priceObject[type].count = details;          
				}
				// productColor, productTexture, productPattern
				else {
					priceObject[type].count = objectKeyCount(details);
				}
				priceObject[type].perPrice = price;
				priceObject[type].total = priceObject[type].count*price;
			});

			return priceObject;
		}
		
		designStudio.updatePrice = function(){
			var qty = 0;
			var total = 0;
			var sizes = ['xs', 's', 'm', 'l', 'xl', 'xxl'];
			
			$.each(designStudio.calculatePrice(), function(name, detailsObject){
				if(detailsObject.count) total += detailsObject.total;
			});

			// Quantities
			$.each(sizes, function(i, size){
				var sizeCount =  parseInt($.trim($('.'+size+'_count').val()));
				sizeCount = isNaN(sizeCount) ? 0 : sizeCount;
				qty += sizeCount;

				$('.'+size+'_total').text(sizeCount*total);
			})      
			//console.log(qty);

			// If quantity is 0, take qty as 1
			if(!qty) qty = 1;


			$(".total_price").text(qty*total.toFixed(2));
		}

		designStudio.resetAddons = function(){
			$.fn.designStudio.defaultOptions.addons = {
				text: 0,
				image: 0,
				productFill: {},
				productTexture: {},
				productPattern: {}
			};	
		}

		designStudio.resetPrice = function(){			
			designStudio.resetAddons();
			designStudio.updatePrice();
		}

		designStudio.addText = function(src, params){
			designStudio.addElement("text", src, $.extend({text: src}, params));
			designStudio.modifyAddons("add", "text", {src: src});
			designStudio.updatePrice();
		}

		designStudio.addImage = function(src, params){
			designStudio.addElement("image", src, params);
			designStudio.modifyAddons("add", "image", {src: src});
			designStudio.updatePrice();
		}

		designStudio.changeTextProperty = function(property, value){
			if(typeof currentElement === "undefined" || (currentElement.type !== "i-text" && currentElement.type !== "text")) return;
			switch(property) {
				case "bold":
					_setStyle(currentElement, 'fontWeight', currentElement.getFontWeight() == "bold" ? "" : "bold");
					break 

				case "italic":
					_setStyle(currentElement, 'fontStyle', currentElement.getFontStyle() == "italic" ? "" : "italic");
					break;

				case "underline":
					_setStyle(currentElement, 'textDecoration', currentElement.getTextDecoration() == "underline" ? "" : "underline");
					break;

				case "align-left":
					currentElement.setTextAlign("left");
					currentElement.params.textAlign = "left";
					break;

				case "align-center":
					currentElement.setTextAlign("center");
					currentElement.params.textAlign = "center";
					break;

				case "align-right":
					currentElement.setTextAlign("right");
					currentElement.params.textAlign = "right";
					break;

				case "font-family":
					currentElement.setFontFamily(value);
					currentElement.params.font = value;
					break;

				case "font-fill":
					currentElement.setFill(value);
					currentElement.params.fill = value;
					break;

				case "line-height":
					currentElement.setLineHeight(value);
					currentElement.params.lineHeight = value;
					break;

				case "opacity":
					currentElement.setOpacity(value);
					currentElement.params.opacity = value;
					break;

			}

			stage.renderAll();
		}

		designStudio.changeImageProperty = function(property, value){
			//if(currentElement.params.base || currentElement.type != "image") return;
			switch(property) {
				case "color":
					_changeImageColor(currentElement, value);
					break;
			}
		}

		designStudio.changeProductProperty = function(property, value){
			switch(property) {
				case "fill":
					designStudio.changeAllSidesColor(value);			
					designStudio.modifyAddons('add', 'productFill', {name: 'base', src: value});
					break;
			}
			designStudio.updatePrice();
			stage.renderAll();
			setTimeout(designStudio.setPreviewImages, 500);
		}

		designStudio.changeAllSidesColor = function(color){
			$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
				window[side+"Base"].filters = [];
				window[side+"Base"].filters.push(new fabric.Image.filters.Tint({color: color, opacity: 1}));
				try {
					var currentSide = window[side];
					//console.log(side+"Base - "+window[side+"Base"]);
					window[side+"Base"].applyFilters(currentSide.renderAll.bind(currentSide));
				}
				catch(evt) {
					alert("Tshirt cannot be color. Please check if image is hosted under same domain");
				}
			});
			setTimeout(function(){designStudio.setPreviewImages()}, 200);
		}

		designStudio.reset = function(){
			designStudio.resetPrice();
			$(container).children('div').each(function(){
				var $productContainer = $(this);
				var currentSide = $productContainer.data('side');
				window[currentSide].clear();
				_addDefaultParams($productContainer.data('params'), currentSide);
			})
		}

		designStudio.changeProduct = function(){
			_init();
		}

		designStudio.getPreviewImages = function(){
			var previewImages =  {};
			var currActiveObject = stage.getActiveObject();
			//front: front.deactivateAll().renderAll().toDataURL(), back: back.deactivateAll().renderAll().toDataURL(), left: left.deactivateAll().renderAll().toDataURL(), right: right.deactivateAll().renderAll().toDataURL()
			$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
				previewImages[side] = window[side].deactivateAll().renderAll().toDataURL();
			})
			if(currActiveObject) stage.setActiveObject(currActiveObject);
			return previewImages;
		}

		designStudio.setPreviewImages = function(){
			var previewImages = designStudio.getPreviewImages();
			$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
				$("#"+side+"-preview").attr("src", previewImages[side]);
			})
		}
		/* Public Methods Ended */

		/* Helpers Started */
		function cl(data){
			console.log(data);
		}

		function dd(data){
			cl(JSON.stringify(data, null, 4));
		}
		/* Helpers Ended */

	}

	$.fn.designStudio = function( options ){
		return this.each(function(){
			var element = $(this);
			if(element.data('design-studio')) return;
			var designStudioInstance = new designStudio(this, options);
			element.data('design-studio', designStudioInstance);
		});
	}

	// Default Parameters
	$.fn.designStudio.defaultOptions = {
		sides: [],
		price: {
			base: 10,
			text: 3,
			image: 5,
			productFill: 6,
			productTexture: 7,
			productPattern: 8 
		},
		addons: {
			text: 0,
			image: 0,
			productFill: {},
			productTexture: {},
			productPattern: {}
		},
		dimensions: {
			productDisplayWidth: 369,
			productDisplayHeight: 430,
			productStageWidth: 369,
			productStageHeight: 430,
		},
		elementParameters: {
			x: 0,
			y: 0,
			z: -1,
			colors: false,
			removable: false,
			draggable: false,
			rotatable: false,
			resizable: false,
			zChangeable: false,
			scale: 1,
			degree: 0,
			price: 0,
			boundingBox: false,
			autoCenter: false,
			opacity: 1,
			originX: 'center',
			originY: 'center',
			replace: '',
			boundingBoxClipping: false,
			autoSelect: false,
			topped: false,
			uploadZone: false
		},
		textParameters: {
			font: false,
			fontWeight: '',
			fontStyle: '',
			textSize: 40,
			patternable: false,
			editable: true,
			lineHeight: 1,
			textAlign: 'left',
			textBackgroundColor: '',
			textDecoration: '',
			maxLength: 0,
			curved: false,
			curvable: false,
			curveSpacing: 10,
			curveRadius: 80,
			curveReverse: false
		},
		defaultTextParameters: {
			lot: {
				x: 550,
				y: 100,
				text: "Lotto 1/11"
			}
		},
		customImagesParameters: {
			minW: 50,
			minH: 100,
			maxW: 300,
			maxH: 400,
			resizeToW: 300,
			resizeToH: 400
		}		
	}
	
})(jQuery);
