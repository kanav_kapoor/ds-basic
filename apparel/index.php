<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>DS Basic - Apparel</title>
		<!-- CSS -->
		
		<!-- plugins -->
		<link rel="stylesheet" href="assets/lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.min.css">
		<link href='http://fonts.googleapis.com/css?family=Josefin+Sans|Shadows+Into+Light|Pacifico|Quicksand|Architects+Daughter|Dancing+Script|Chewy|Gloria+Hallelujah|Shadows+Into+Light|Poiret+One|Indie+Flower|Oswald|Play' rel='stylesheet' type='text/css'>

		<!-- <link rel="stylesheet" href="assets/css/fonts.css"> -->
		<link rel="stylesheet" href="assets/css/menu.css">
		<!--/ plugins -->

		<link rel="stylesheet" href="assets/css/index.css">
		<link rel="stylesheet" href="assets/css/media.css">
		<!--/ CSS -->
	</head>
	<body>
		<div id="loader">
			<p class="text-center"><br>Loading.... Please wait...</p>
		</div>
		<!-- header -->
		<header>
			<!-- Nav -->
			<nav class="animate focus">
				<ul class="nav nav-pills nav-stacked tool-options" role="tablist">
					<li role="presentation" class="active"><a class="text-center" title="Products" data-toggle="tab" aria-controls="tabpane-products" href="#tabpane-products"><i class="fa fa-th"></i></a></li>
					<li role="presentation"><a class="text-center" title="Product Options" data-toggle="tab" aria-controls="tabpane-product-options" href="#tabpane-product-options"><i class="fa fa-adjust"></i></a></li>
					<li role="presentation"><a class="text-center" title="Text Options" data-toggle="tab" aria-controls="tabpane-text-options" href="#tabpane-text-options"><i class="fa fa-pencil"></i></a></li>
					<li role="presentation"><a class="text-center" title="Image Options" data-toggle="tab" aria-controls="tabpane-image-options" href="#tabpane-image-options"><i class="fa fa-picture-o"></i></a></li>
					<li role="presentation"><a class="text-center" title="Price Calculation" data-toggle="tab" aria-controls="tabpane-price-calculation-options" href="#tabpane-price-calculation-options"><i class="fa fa-cart-plus"></i></a></li>
				</ul>
			</nav>
			<!--/ Nav -->
			<div class="nav-controller hidden">
				<span class="[ ion-2x ion-navicon-round ] controller-open"></span>
				<span class="[ ion-2x ion-ios-close-outline ] controller-close"></span>
			</div>
		</header>
		<!--/ header -->

		<!--  Main Container -->
		<div class="content-wrapper container-fluid">
			<div class="row">

				<!-- Tools -->
				<div class="row tools">
					<div class="col-sm-8 col-md-10 col-sm-offset-10 col-md-offset-10">
						<div class="btn-group btn-group-justified" role="group" aria-label="">
							<div class="btn-group" role="group">
								<button title="Reset" type="button" data-toggle="confirmation" class="btn btn-default" id="reset"><i class="ion-2x ion-ios-loop"></i></button>
							</div>
							<div class="btn-group" role="group">
								<button title="Save" type="button" data-toggle="tooltip" data-placement="bottom" title="Save" class="btn btn-default" id="saveProduct"><i class="ion-2x ion-ios-heart-outline"></i></i></button>
							</div>
						</div>
					</div>
				</div>
				<!--/ Tools -->

				<!-- Design Tool -->
				<div class="row">
					<!-- Left -->
					<div class="col-sm-10 col-md-10">
						<div class="col-sm-16 col-md-14 col-lg-12">
							<!-- Tab Panes -->
							<div class="tab-content tool-panes">
								<!-- Products -->
								<div role="tabpanel" class="products tab-pane fade in active" id="tabpane-products">
									<p class="custom-pane-header text-center h3">Products</p><hr>
									<div class="row display-gallery products-list">
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/cap-01/cap-thumbnail.png" alt="cap " title="cap">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/cap-01/front-preview.png", "base": "assets/img/products/cap-01/front-base.png","template":[{"type": "text", "params": {"src": "INDIA", "editorMode": true, "draggable": true, "x": 191, "y": 140, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#FF6633"], "textSize": 40}},{"type": "image", "params": {"src": "assets/img/sample/stars.png","editorMode": true, "draggable": true, "resizeToW": 28, "x": 201, "y":179}},{"type": "image", "params": {"src": "assets/img/sample/stars.png","editorMode": true, "draggable": true, "resizeToW": 28, "x": 230, "y":180}},{"type": "image", "params": {"src": "assets/img/sample/stars.png","editorMode": true, "draggable": true, "resizeToW": 28, "x": 261, "y":179}},{"type": "text", "params": {"src": "CRICKET", "editorMode": true, "draggable": true, "x": 197, "y": 205, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#128807"], "textSize": 23}},{"type": "image", "params": {"src": "assets/img/sample/bcci.png","editorMode": true, "draggable": true, "resizeToW": 73, "x": 208, "y":230}}]}'>
												</div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/cap-01/back-preview.png", "base": "assets/img/products/cap-01/back-base.png", "template": [{"type": "image", "params": {"src": "assets/img/sample/cricket.png","editorMode": true, "draggable": true, "resizeToW": 129, "x": 113, "y":130}},{"type": "image", "params": {"src": "assets/img/sample/ball.png","editorMode": true, "draggable": true, "resizeToW": 48, "x": 262, "y":143}},{"type": "text", "params": {"src": "ICC", "editorMode": true, "draggable": true, "x": 310, "y": 146, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans", "colors": ["#231F20"], "textSize": 20}},{"type": "text", "params": {"src": "CRICKET", "editorMode": true, "draggable": true, "x": 272, "y": 174, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans", "colors": ["#231F20"], "textSize": 22}},{"type": "text", "params": {"src": "WORLD", "editorMode": true, "draggable": true, "x": 274, "y": 197, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#231F20"], "textSize": 20}},{"type": "text", "params": {"src": "CUP", "editorMode": true, "draggable": true, "x": 277, "y": 219, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#7C7C11"], "textSize": 20}},{"type": "text", "params": {"src": "2016", "editorMode": true, "draggable": true, "x": 277, "y": 244, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#7C7C11"], "textSize": 20}},{"type": "text", "params": {"src": "World Cup", "editorMode": true, "draggable": true, "x": 167, "y": 357, "textAlign": "center","degree":2,"fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#7C7C11"], "textSize": 20}},{"type": "text", "params": {"src": "2016", "editorMode": true, "draggable": true, "x": 269, "y": 359,"degree":353, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#7C7C11"], "textSize": 20}}]}'>
												</div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->						
										
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/cap-01/cap-thumbnail.png" alt="cap " title="cap">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/cap-01/front-preview.png", "base": "assets/img/products/cap-01/front-base.png","template": [
												{"type": "image", "params": {"src": "assets/img/sample/logo-01.png","editorMode": true, "draggable": true, "resizeToW": 123, "x": 186, "y":145}},{"type": "text", "params": {"src": "WEST INDIES", "editorMode": true, "draggable": true, "x": 198, "y": 264, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#891C2F"], "textSize": 15}},{"type": "text", "params": {"src": "CRICKET BOARD", "editorMode": true, "draggable": true, "x": 194, "y": 283, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#891C2F"], "textSize": 15}},{"type": "text", "params": {"src": "WEST", "editorMode": true, "draggable": true, "x": 156, "y": 362,"degree":354, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#C48710"], "textSize": 30}},{"type": "text", "params": {"src": "INDIES", "editorMode": true, "draggable": true, "x": 254, "y": 353,"degree":8, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#C48710"], "textSize": 30}}]}'>
												</div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/cap-01/back-preview.png", "base": "assets/img/products/cap-01/back-base.png", "template": [{"type": "image", "params": {"src": "assets/img/sample/cricket.png","editorMode": true, "draggable": true, "resizeToW": 129, "x": 113, "y":130}},{"type": "image", "params": {"src": "assets/img/sample/ball.png","editorMode": true, "draggable": true, "resizeToW": 48, "x": 261, "y":143}},{"type": "text", "params": {"src": "ICC", "editorMode": true, "draggable": true, "x": 304, "y": 146, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans", "colors": ["#231F20"], "textSize": 20}},{"type": "text", "params": {"src": "CRICKET", "editorMode": true, "draggable": true, "x": 272, "y": 174, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans", "colors": ["#231F20"], "textSize": 22}},{"type": "text", "params": {"src": "WORLD", "editorMode": true, "draggable": true, "x": 274, "y": 197, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#231F20"], "textSize": 20}},{"type": "text", "params": {"src": "CUP", "editorMode": true, "draggable": true, "x": 277, "y": 219, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#7C7C11"], "textSize": 20}},{"type": "text", "params": {"src": "2016", "editorMode": true, "draggable": true, "x": 277, "y": 244, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#7C7C11"], "textSize": 20}}]}'>
												</div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/muffler-01/muffler-thumbnail.png" alt="muffler " title="muffler">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/muffler-01/front-preview.png", "base": "assets/img/products/muffler-01/front-base.png","template": [{"type": "image", "params": {"src": "assets/img/sample/logo-02.png","editorMode": true, "draggable": true, "resizeToW": 175, "x": 321, "y": 139,"degree":27}},{"type": "text", "params": {"src": "Vivienne", "editorMode": true, "draggable": true, "x": 282, "y": 239,"degree":26, "textAlign":"center","fontWeight":"bold","font": "Dancing Script ", "colors": ["#B3042B"], "textSize": 22}},{"type": "text", "params": {"src": "Westwood", "editorMode": true, "draggable": true, "x": 268, "y": 261,"degree":28, "textAlign":"center","fontWeight":"bold","font": "Dancing Script ", "colors": ["#B3042B"], "textSize": 22}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 41, "x": 96, "y": 183}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 53, "x": 122, "y": 253}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 41, "x": 187, "y": 334}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 43, "x": 135, "y": 71}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 46, "x": 205, "y": 108}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 41, "x": 96, "y": 183}},{"type": "image", "params": {"src": "assets/img/sample/flowers1.png","editorMode": true, "draggable": true, "resizeToW": 44, "x": 244, "y": 159}}]}'>
												</div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/muffler-01/muffler-thumbnail.png" alt="muffler " title="muffler">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/muffler-01/front-preview.png", "base": "assets/img/products/muffler-01/front-base.png","template": [{"type": "text", "params": {"src": "N", "editorMode": true, "draggable": true, "x": 156, "y": 138,"degree":315, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#B3042B"], "textSize": 55}},{"type": "text", "params": {"src": "I", "editorMode": true, "draggable": true, "x": 197, "y": 161,"degree":315, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#B3042B"], "textSize": 55}},{"type": "text", "params": {"src": "K", "editorMode": true, "draggable": true, "x": 218, "y": 198,"degree":315, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#B3042B"], "textSize": 55}},{"type": "text", "params": {"src": "E", "editorMode": true, "draggable": true, "x":252, "y": 231,"degree":315, "textAlign":"center","fontWeight":"bold","font": "Play", "colors": ["#B3042B"], "textSize": 55}},{"type": "image", "params": {"src": "assets/img/sample/nike.png","editorMode": true, "draggable": true, "resizeToW": 144, "x": 252, "y": 278,"degree":298}},{"type": "text", "params": {"src": "Nike", "editorMode": true, "draggable": true, "x": 174, "y": 239,"degree":46, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#B3042B"], "textSize": 25}},{"type": "image", "params": {"src": "assets/img/sample/nike.png","editorMode": true, "draggable": true, "resizeToW": 70, "x":163, "y": 248,"degree":43}}]}'>
												</div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
									</div>
								</div>
								<!--/ Products -->

								<!-- Product Options Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-product-options">
									<p class="custom-pane-header text-center h3">Product Options</p><hr>
									<div class="row display-gallery base-colors">
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#A2C44C" style="background-color: #A2C44C"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#28272c" style="background-color: #28272c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#e975a4" style="background-color: #e975a4"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#891c2f" style="background-color: #891c2f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#6e95ce" style="background-color: #6e95ce"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#b3042b" style="background-color: #b3042b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#7d5b52" style="background-color: #7d5b52"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#fed348" style="background-color: #fed348"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#3c3230" style="background-color: #3c3230"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#333a32" style="background-color: #333a32"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#feac1c" style="background-color: #feac1c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#d1316f" style="background-color: #d1316f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#edb963" style="background-color: #edb963"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#c2bebf" style="background-color: #c2bebf"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#4a5d7b" style="background-color: #4a5d7b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#497bba" style="background-color: #497bba"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#00995b" style="background-color: #00995b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#01858a" style="background-color: #01858a"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#008767" style="background-color: #008767"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#85a35d" style="background-color: #85a35d"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#9cbad6" style="background-color: #9cbad6"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#f0c6dc" style="background-color: #f0c6dc"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#83cb67" style="background-color: #83cb67"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#612b39" style="background-color: #612b39"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#FFFFFF" style="background-color: #FFFFFF"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#304972" style="background-color: #304972"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#5a5947" style="background-color: #5a5947"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#e7dbcb" style="background-color: #e7dbcb"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#304050" style="background-color: #304050"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#282b3c" style="background-color: #282b3c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#564d46" style="background-color: #564d46"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#FF6633" style="background-color: #FF6633"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#b8a8cd" style="background-color: #b8a8cd"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#C8D8AB" style="background-color: #C8D8AB"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#C48710" style="background-color: #C48710"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-color="#663D50" style="background-color: #663D50"></div></div>
									</div>
								</div>
								<!--/ Product Options Pane -->
								
								<!-- Text Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-text-options">
									<p class="custom-pane-header text-center h3">Text Options</p><hr>
									<button id="btn-add-text" type="button" class="btn btn-default btn-lg btn-block">Add Text</button>
									<br/>

									<select class="form-control input-lg" id="text-font-family">
										<!-- <option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
										<option style="font-family:Courier New" value="Courier New">Courier New</option>
										<option style="font-family:Comic Sans MS" value="Comic Sans MS">Comic Sans MS</option>
										<option style="font-family:Times New Roman" value="Times New Roman">Times New Roman</option>
										<option style="font-family:Palatino Linotype" value="Palatino Linotype">Palatino Linotype</option>
										<option style="font-family:Impact" value="Impact">Impact</option>
										<option style="font-family:Arial" value="Arial">Arial</option>
										<option style="font-family:Tahoma" value="Tahoma">Tahoma</option>
										<option style="font-family:Arial Black" value="Arial Black">Arial Black</option>
										<option style="font-family:Verdana" value="Verdana">Verdana</option>
										<option style="font-family:MS Sans Serif" value="MS Sans Serif">MS Sans Serif</option> -->
										
										<option style="font-family:Arial" value="Arial">Arial</option>
										<option style="font-family:Indie Flower" value="Indie Flower">Indie Flower</option>
										<option style="font-family:Poiret One" value="Poiret One">Poiret One</option>
										<option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
										<option style="font-family:Shadows Into Light" value="Shadows Into Light">Shadows Into Light</option>
										<option style="font-family:Pacifico" value="Pacifico">Pacifico</option>
										<option style="font-family:Quicksand" value="Quicksand">Quicksand</option>
										<option style="font-family:Architects Daughter" value="Architects Daughter">Architects Daughter</option>
										<option style="font-family:Dancing Script" value="Dancing Script">Dancing Script</option>
										<option style="font-family:Chewy" value="Chewy">Chewy</option>
										<option style="font-family:Gloria Hallelujah" value="Gloria Hallelujah">Gloria Hallelujah</option>

									</select>
									<br>
									<div class="btn-lg color-selector">
										<div class="pull-left">Color</div>
										<div class="pull-right">
											<div class="color-preview"></div>
										</div>
										<div class="clearfix"></div>
									</div>									
									<div class="display-gallery hidden" id="text-font-fill">
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
									</div>
									<div class="clearfix"></div>
									<br>
									
									<div class="btn-group btn-group-justified" role="group" id="text-properties">
										<div class="btn-group" role="group">
											<button type="button" id="text-bold" class="btn btn-default"><i class="fa fa-bold"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="text-italic" class="btn btn-default"><i class="fa fa-italic"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="text-underline" class="btn btn-default"><i class="fa fa-underline"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="text-align-left" class="btn btn-default"><i class="fa fa-align-left"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="text-align-center" class="btn btn-default"><i class="fa fa-align-center"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="text-align-right"  class="btn btn-default"><i class="fa fa-align-right"></i></button>
										</div>
									</div>
								</div>
								<!--/ Text Pane -->
								
								<!-- Image Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-image-options">
									<p class="custom-pane-header text-center h3">Image Options</p><hr>
									<div class="btn-lg color-selector">
										<div class="pull-left">Color</div>
										<div class="pull-right">
											<div class="color-preview"></div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="display-gallery hidden" id="image-fill">
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#000000" style="background-color: #000000"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
										<div class="col-xs-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
									</div>
									<div class="clearfix"></div>
									<br>
									<ul class="nav nav-tabs" role="tablist">
										<li class="active"><a role="presentation" href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
										<li><a role="presentation" href="#uploads" aria-controls="uploads" role="tab" data-toggle="tab">Uploads</a></li>
									</ul>

									<div class="tab-content">
										<div role="tabpanel" class="tab-pane fade in active" id="gallery">
											<div class="row display-gallery">
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/nike.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/adidas.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/being-human.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/puma.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/tattoos-01.png" alt=""></div>
												
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/camera-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/dragon-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/hearts-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/happy-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/sad-01.png" alt=""></div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="uploads">
											<div class="row display-gallery">
												<div class="col-sm-24 text-center">
													<br>
													<button id="btn-add-img" type="button" class="btn btn-default btn-lg"><i class="ion-ios-upload-outline"></i> Upload Image</button>
													<input type="file" id="add-img" style="display: none;" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Image Pane -->

								<!-- Cart Calculations Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-price-calculation-options">
										<p class="custom-pane-header text-center h3">Price Calculations</p><hr>
										<div class="row">
											<div class="col-xs-12 col-sm-16 col-md-24">
												<!-- table -->
												<table class="table table-hover">
													<thead>
														<tr>
															<th>Size</th>
															<th class="text-center">#</th>
															<th class="text-center">Total</th>
														</tr>
													</thead>
													<tbody>
														<tr data-toggle="popover" data-popover-size="xs" title="Name & Number" data-html="true" data-content="<div data-nn-size='xs'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>XS</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="xs" class="sizes xs_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="xs_total">0</span></td>
														</tr>
														<tr data-toggle="popover" data-popover-size="s" title="Name & Number" data-html="true" data-content="<div data-nn-size='s'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>S</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="s" class="sizes s_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="s_total">0</span></td>
														</tr>
														<tr data-toggle="popover" data-popover-size="m" title="Name & Number" data-html="true" data-content="<div data-nn-size='m'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>M</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="m" class="sizes m_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="m_total">0</span></td>
														</tr>												
														<tr data-toggle="popover" data-popover-size="l" title="Name & Number" data-html="true" data-content="<div data-nn-size='l'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>L</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="l" class="sizes l_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="l_total">0</span></td>
														</tr>
														<tr data-toggle="popover" data-popover-size="xl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>XL</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="xl" class="sizes xl_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="xl_total">0</span></td>
														</tr>
														<tr data-toggle="popover" data-popover-size="xxl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xxl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
															<td class="col-sm-6"><em>XXL</em></td>
															<td class="col-sm-2 text-center"><input type="text" data-input-size="xxl" class="sizes xxl_count form-control input-sm"></td>
															<td class="col-sm-2 text-center">$<span class="xxl_total">0</span></td>
														</tr>
														<tr>
															<td></td>													
															<td class="text-center">
																<h4><strong>Total</strong></h4>
															</td>
															<td class="text-center text-danger">
																<h4><strong>$<span class="total_price">0.00</span></strong></h4>	
															</td>
														</tr>
													</tbody>
												</table>
												<!--/ table -->
												<button class="btn btn-lg btn-block btn-default">Add To Cart</button>
											</div>
										</div>
								</div>
								<!--/ Cart Calculations Pane -->
							</div>
							<!--/ Tab Panes -->
							<br>
						</div>
					</div>
					<!--/ Left -->
					
					<div class="col-sm-14 col-md-14">
						<div class="row">
							<!-- Center -->
							<div class="col-sm-14 col-md-18">
								<!-- Design Tool Stage-->
								<div class="col-xs-24">
									<div class="text-center stage" id="design-studio">
										<div data-side="front" data-params='{"preview": "assets/img/products/cap-01/front-preview.png", "base": "assets/img/products/cap-01/front-base.png","template":[{"type": "text", "params": {"src": "INDIA", "editorMode": true, "draggable": true, "x": 191, "y": 140, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#FF6633"], "textSize": 40}},{"type": "image", "params": {"src": "assets/img/sample/stars.png","editorMode": true, "draggable": true, "resizeToW": 28, "x": 201, "y":179}},{"type": "image", "params": {"src": "assets/img/sample/stars.png","editorMode": true, "draggable": true, "resizeToW": 28, "x": 230, "y":180}},{"type": "image", "params": {"src": "assets/img/sample/stars.png","editorMode": true, "draggable": true, "resizeToW": 28, "x": 261, "y":179}},{"type": "text", "params": {"src": "CRICKET", "editorMode": true, "draggable": true, "x": 197, "y": 205, "textAlign":"center","fontWeight":"bold","font": "Play ", "colors": ["#128807"], "textSize": 23}},{"type": "image", "params": {"src": "assets/img/sample/bcci.png","editorMode": true, "draggable": true, "resizeToW": 73, "x": 208, "y":230}}]}'>
										</div>
										<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/cap-01/back-preview.png", "base": "assets/img/products/cap-01/back-base.png", "template": [{"type": "image", "params": {"src": "assets/img/sample/cricket.png","editorMode": true, "draggable": true, "resizeToW": 129, "x": 113, "y":130}},{"type": "image", "params": {"src": "assets/img/sample/ball.png","editorMode": true, "draggable": true, "resizeToW": 48, "x": 262, "y":143}},{"type": "text", "params": {"src": "ICC", "editorMode": true, "draggable": true, "x": 310, "y": 146, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans", "colors": ["#231F20"], "textSize": 20}},{"type": "text", "params": {"src": "CRICKET", "editorMode": true, "draggable": true, "x": 272, "y": 174, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans", "colors": ["#231F20"], "textSize": 22}},{"type": "text", "params": {"src": "WORLD", "editorMode": true, "draggable": true, "x": 274, "y": 197, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#231F20"], "textSize": 20}},{"type": "text", "params": {"src": "CUP", "editorMode": true, "draggable": true, "x": 277, "y": 219, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#7C7C11"], "textSize": 20}},{"type": "text", "params": {"src": "2016", "editorMode": true, "draggable": true, "x": 277, "y": 244, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#7C7C11"], "textSize": 20}},{"type": "text", "params": {"src": "World Cup", "editorMode": true, "draggable": true, "x": 167, "y": 357, "textAlign": "center","degree":2,"fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#7C7C11"], "textSize": 20}},{"type": "text", "params": {"src": "2016", "editorMode": true, "draggable": true, "x": 269, "y": 359,"degree":353, "textAlign": "center","fontWeight":"bold","font": "Josefin Sans  ", "colors": ["#7C7C11"], "textSize": 20}}]}'>
										</div>
									</div>
								</div>
								<!--/ Design Tool Stage-->
							</div>
							<!--/ Center -->

							<!-- Right -->
							<div class="col-sm-10 col-md-6">
								<!-- Preview Images -->
								<div class="col-sm-12 text-center preview-images">
									<div class="row">
										<div class="col-xs-6 col-sm-24">
											<img src="" class="img-thumbnail" id="front-preview" alt="Front">
										</div>
										<div class="col-xs-6 col-sm-24">
											<img src="" class="img-thumbnail" id="back-preview" alt="Back">
										</div>
									</div>							
								</div>
								<!--/ Preview Images -->
							</div>
							<!--/ Right -->
						</div>
						<div class="row">
							<!-- Price -->
							<div class="col-sm-10 col-sm-offset-14 col-md-6 col-md-offset-18">
								<div class="col-sm-12 text-center total_price_col">
									<br>
									<div class="well well-sm"><h4>$<span class="total_price">0.00</span></h4></div>
								</div>
							</div>
							<!--/ Price -->
						</div>
					</div>
				</div>
				<!--/ Design Tool -->

				<!-- Price -->
				<!-- <div class="row">
					<div class="col-sm-6 col-md-4 col-sm-offset-18 col-md-offset-20">
						<span class="visible-xs"><br><br><br><br><br><br></span>
						<div class="col-sm-12 text-center total_price_col">
							<br>
							<div class="well well-sm"><h4>$<span class="total_price">0.00</span></h4></div>
						</div>
					</div>
				</div> -->
				<!--/ Price -->
			</div>
		</div>
		<!--/  Main Container -->
		<!-- Pre-load Fonts -->
		<section class="preload-fonts"></section>
		<!--/ Pre-load Fonts -->
		<!-- JS -->
		<!-- plugins -->
		<script type="text/javascript" src="assets/lib/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/lib/confirmation/bootstrap-confirmation.min.js"></script>
		<script type="text/javascript" src="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="assets/lib/fabric/fabric.min.js"></script>		
		<script type="text/javascript" src="assets/lib/design-studio/design-studio.js"></script>
		<!--/ plugins -->
		<script type="text/javascript" src="assets/js/index.js"></script>
		<!--/ JS -->
	</body>
</html>