(function($){
	$.noConflict();
	$(function($) {
		/* Initialize Plugins - Start */
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle=confirmation]').confirmation({placement: "bottom", popout: true});

		$("body, .display-gallery").mCustomScrollbar({
			theme:"minimal-dark",
			autoHideScrollbar: true,
		});

		_preloadFonts();
		/* Initialize Plugins - End */

		$('html').on("mouseup", function(event) {
			// Hide Display Gallery
			if(!$(event.target).closest('.color-selector + .display-gallery').length && !$(event.target).closest('.color-selector').length)
					$('.color-selector + .display-gallery').addClass('hidden');

			hideAllPopovers(event);
		});

		// Color/Pattern Selector
		$(".color-selector").on("click", function(){
			$(this).next().toggleClass("hidden");
			$('.color-selector + .display-gallery').not($(this).next()).addClass('hidden');
		});


		$("#close").on("click", function(){
			$("a[data-toggle='tab']:first").tab("show");
		});
		// Tab Sliding Effect
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			if(!$(this).closest('ul').hasClass('tool-options')) return;
			
			var target = $(this).attr('href');

			$(target).css('top','-'+$(window).width()+'px');   
			var top = $(target).offset().top;
			$(target).css({top:top}).animate({"top":"0px"}, 800);
		});

		var _preloadImages = function(images){
			$.each(images, function(i, img){
				(new Image()).src = img;
			});
		}

		function _preloadFonts(){
			$("#text-font-family").children().each(function(){
				$('.preload-fonts').append("<p style='"+$(this).attr('style')+"'>Loading...</p>");
			});
			$('.preload-fonts').hide();
		}

		function onlyUnique(value, index, self){
			return self.indexOf(value) === index;
		}
		
		var productImages = [];
		$('.product-specs [data-params]').each(function(){
			var dataParams = JSON.parse($(this).attr('data-params'));

			productImages.push(dataParams.preview);
			productImages.push(dataParams.base);
		});

		$("[data-src]").each(function(){
			if($(this).data('src')) productImages.push($(this).data('src'));
		});

		productImages = productImages.filter(onlyUnique);
		_preloadImages(productImages);
		
		// Toggle nav
		function toggleNav(){
			// Large Screen
			if($(window).width() >= 768) {
				$(".nav-controller").addClass('hidden');
				$("nav").addClass('focus');
			}
			// Small Screen
			else {
				$(".nav-controller").removeClass('hidden');
				$("nav").removeClass('focus');
			}
		}

		// Hide Navbar on click -  small screens
		$('nav, .nav-controller').on('click', function(event) {
			if($(window).width() < 768) $('nav').toggleClass('focus');
		});

		$(window).on("resize load", toggleNav);

		var $stageContainer = $("#design-studio");

		var designStudio = $stageContainer.designStudio({dimensions: {productDisplayWidth: $stageContainer.width(), productStageWidth: 500, productStageHeight: 500}}).data('design-studio');

		$("#reset").on("click", function(){
			designStudio.reset();
		});

		// Change Sides
		$(".preview-images img").on("click", function(){
			designStudio.changeSide($(this).attr('id').replace("-preview", ""));
		})

		// Add Text
		$("#btn-add-text").on("click", function(){
			var defaultText = "New Text";
			var font = $("#text-font-family").val();			
			designStudio.addText('New Text', {font: font, autoCenter: true, editorMode: true, draggable: true, autoSelect: true})
		});

		// Modify Text
		$("#text-font-family").on("change",  function(){
			var fontFamily = $(this).val();
			designStudio.changeTextProperty('font-family', fontFamily);
			$(this).css("font-family", fontFamily);
		});

		$("#text-font-fill div[data-fill]").on("click",  function(){
				var fontFill = $(this).data("fill");
				designStudio.changeTextProperty('font-fill', fontFill);
		});

		$("#text-bold").on("click", function(){
			designStudio.changeTextProperty('bold');
		});

		$("#text-italic").on("click", function(){
			designStudio.changeTextProperty('italic');
		});

		$("#text-underline").on("click", function(){
			designStudio.changeTextProperty('underline');
		});

		$("#text-align-left").on("click", function(){
			designStudio.changeTextProperty('align-left');
		});

		$("#text-align-center").on("click", function(){
			designStudio.changeTextProperty('align-center');
		});

		$("#text-align-right").on("click", function(){
			designStudio.changeTextProperty('align-right');
		});

		// Register Add Image Handler
			$("#btn-add-img").on("click", function(e){
				$('#add-img').trigger('click');
				e.preventDefault();
			});

			// Add Image
			$("#add-img").on("change", function(e){
				var reader = new FileReader();
				reader.readAsDataURL(e.target.files[0]);
				reader.onload = function(e){
					designStudio.addImage(e.target.result, {autoCenter: true, editorMode: true, draggable: true, autoSelect: true});
					
					// Add image to gallery
					if($("img[src='"+e.target.result+"']").length) return;
					$("#uploads > .row").append("<div class='col-xs-8 col-md-6 top5'><img height=120 width=120 class='img-thumbnail' src='"+e.target.result+"' alt=''></div>");
				}
			});

			// Load from gallery
			$("div").on("click", "#gallery img, #uploads img", function(e){
					designStudio.addImage($(this).attr("src"), {autoCenter: true, editorMode: true, draggable: true, autoSelect: true});
					e.stopPropagation();
			})

			// Change Image Color
			$("#image-fill div[data-fill]").on("click",  function(){
				var imageFill = $(this).data("fill");
				designStudio.changeImageProperty('color', imageFill);
			});

			// Change Base Color
			$(".base-colors div[data-color]").on("click", function(){				
				designStudio.changeProductProperty('fill', $(this).data('color'));
			})

			// Change Product
			$(".products-list .product").on("click", function(){
				$("#design-studio").html($(this).children(".product-specs").html());
				designStudio.changeProduct();
			});



			/* Calculations  - Start */
			$(".sizes").on({"change keyup paste": sizeChanged, "click focus": showPopover});

			function sizeChanged(e){
				designStudio.updatePrice();
				changeNameNumberInputs(e);
				showPopover(e);
			}

			function showPopover(e){
				var $this = $(e.target).closest("tr");
				$('.sizes').closest("tr").not($this).popover('hide');
				$this.popover({"trigger": "manual", container: "body"});
				$this.popover('show');
			}


			function changeNameNumberInputs(e){
				var $elem = $(e.target);
				var value = $.trim($elem.val());
				// If value is numeric or null
				if(!isNaN(value) && value !=""){
					
					var size = $elem.data('input-size');
					// popover containing settings
					var $popover = $elem.closest("tr");
					// popover that is displayed dynamically
					var $popoverDom = $("[data-nn-size='"+size+"']");

					
					var $childrenDiv = $popoverDom.children('div');
					var existingValue = $childrenDiv.length;
					var rowHtml = "<div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div>";
					
					//console.log(existingValue+" - "+value);
					if(existingValue < value) {
						for(var i=existingValue; i<value;i++){
							$popoverDom.append(rowHtml);
						}
					}else if(existingValue > value){
						 $popoverDom.children('div:gt('+(value-1)+')').remove();
					}else {

					}
					
					var newHtml = $popoverDom.get(0).outerHTML;
					$popover.attr('data-content', newHtml);
				}
				e.stopPropagation();
			}

	

			$("[data-toggle='popover']").on('click keyup', function(){
				$("[data-toggle='popover']").not(this).popover('hide');
			});

			function hideAllPopovers(e){
				$('[data-toggle="popover"]').each(function () {
				//the 'is' for buttons that trigger popups
				//the 'has' for icons within a button that triggers a popup
					if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
						$(this).popover('hide');
					}
				});

			}


			$("[data-toggle='popover']").on('hide.bs.popover', function(){
				var $this = $(this);
				var size = $this.data('popover-size');
				var $popoverDom = $("[data-nn-size='"+size+"']");

				if(!$popoverDom.length) return;

				$popoverDom.find('input').each(function(){
					$(this).attr('value', $(this).val())
				});

				var newHtml = $popoverDom.get(0).outerHTML;
				$this.attr('data-content', newHtml);
			});


			/* Calculations - Ends */

			// Save/Load
			
			$("#saveProduct").on("click", function(){
				$.ajax
				({
						type: "POST",
						url: 'db/save-to-json.php',
						data: {newProductImages: JSON.stringify(designStudio.getPreviewImages())},
						success: function (response) {
							alert('Product Saved Successfully');
						},
						failure: function() {alert("there was some problem with the server")}
				});
			})
	})

})(jQuery)