<?php
	$images = [];
	$newProductImages = json_decode($_POST['newProductImages'], true);
	$dir = "products/";
	$randomStr = time();

	// Move all files
	foreach ($newProductImages as $side => $image) {
		$imgName = $randomStr."-".$side.".png";
		file_put_contents($dir.$imgName, base64_decode(str_replace(' ', '+', str_replace('data:image/png;base64,', '', $image))));	
		$images[$side] = $imgName;
	}

	// Append name in json
	$file = "products.json";
	$existingData = json_decode(file_get_contents($file), true);
	$ip = $_SERVER['REMOTE_ADDR'];

	$existingData[$ip][$randomStr] = $images;
	if(file_put_contents($file, json_encode($existingData))) echo true;
?>